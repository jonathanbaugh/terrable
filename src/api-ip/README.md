# API: IP
This service is an example of how to make calls to other APIs by making use of the [ipify API](https://www.ipify.org/).

Because this endpoint doesn't need any input - it just returns the server's IP address - it 
doesn't require a request module.
