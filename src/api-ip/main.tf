data "http" "ipify" {
  url = "https://api.ipify.org"
}

module "res" {
  source = "../http-response"
  content = {
    ip = data.http.ipify.body
  }
}

output "response" {
  value = module.res.response
}
