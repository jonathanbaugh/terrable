module "req" {
  source  = "../http-request"
  http    = var.http
  headers = var.headers
  payload = var.payload
}

module "res" {
  source = "../http-response"
  content = {
    http    = module.req.http
    method  = module.req.method
    path    = module.req.path
    headers = module.req.headers
    json    = module.req.json
    text    = module.req.text
    uri     = module.req.uri
    query   = module.req.query
    params  = module.req.params
  }
}

output "response" {
  value = module.res.response
}
