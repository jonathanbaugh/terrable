variable "http" {
  default     = ""
  description = "Incoming HTTP line (e.g. \"GET / HTTP/1.1\")"
}

variable "headers" {
  default     = ""
  description = "Incoming HTTP headers"
}

variable "payload" {
  default     = ""
  description = "Incoming HTTP content"
}
