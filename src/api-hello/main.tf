module "req" {
  source  = "../http-request"
  http    = var.http
  headers = var.headers
  payload = var.payload
}

locals {
  # Accept either the JSON "name" field or the name parameter
  name = module.req.json != null ? lookup(module.req.json, "name", var.default) : lookup(module.req.params, "name", var.default)
}

module "res" {
  source          = "../http-response"
  content         = { message = "Hello, ${local.name}!" }
}

output "response" {
  value = module.res.response
}
