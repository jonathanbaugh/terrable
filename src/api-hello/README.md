# API: Hello
This API will tell you hello!

### JSON Request
```
$ http POST localhost:1500 name=Jonathan
HTTP/1.1 200 OK
Content-Type: application/json

{
    "message": "Hello, Jonathan!"
}
```

### Parameter Request
```
http 'localhost:1500/?name=Jonathan'HTTP/1.1 200 OK
Content-Type: application/json

{
    "message": "Hello, Jonathan!"
}
```