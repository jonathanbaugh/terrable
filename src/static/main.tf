locals {
  default_meta = yamlencode(var.default_meta)
  files        = sort(fileset(var.content, "/pages/**/*.html"))
  layout       = file("${var.content}/layout.html")
  files_raw = {
    for f in local.files :
    f => file("${var.content}/${f}")
  }
  pages = [
    for f in local.files :
    map(
      "src", "${var.content}/${f}",
      "dest", "${var.dest}${replace(dirname(f), "/pages/", "")}/${basename(f)}",
      "meta", coalesce(trimspace(element(split("---", local.files_raw[f]), 0)), local.default_meta),
      "content", coalesce(trimspace(element(split("---", local.files_raw[f]), 1)), "Empty page."),
    )
  ]
}

data "template_file" "pages" {
  count    = length(local.pages)
  template = local.pages[count.index].content
  vars     = yamldecode(local.pages[count.index].meta)
}

resource "local_file" "pages" {
  count    = length(local.pages)
  filename = local.pages[count.index].dest
  content = templatefile(
    "${var.content}/${data.template_file.pages[count.index].vars.extends}",
    {
      meta    = yamldecode(local.pages[count.index].meta)
      content = data.template_file.pages[count.index].rendered
    }
  )
}
