variable "content" {
  default = "../../content"
}

variable "dest" {
  default = "../../public"
}

variable "default_meta" {
  default = {
    title   = "Terrable example"
    extends = "layout.html"
  }
}
