locals {
  assets = [
    for f in sort(fileset(var.content, "/assets/**/*")) :
    map(
      "src", "${var.content}/${f}",
      "dest", "${var.dest}${replace(dirname(f), "/assets/", "")}/${basename(f)}",
    )
  ]
}

resource "local_file" "assets" {
  count          = length(local.assets)
  filename       = local.assets[count.index].dest
  content_base64 = filebase64(local.assets[count.index].src)
}
