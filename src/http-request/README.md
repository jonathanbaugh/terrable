# Module: HTTP Request
This module will simplify parsing the incoming HTTP requests.

### Variables
| Name | Type | Description |
|-- |-- |-- |
| http | string | Incoming HTTP line (e.g. \"GET / HTTP/1.1\") |
| headers | string | Incoming HTTP headers |
| payload | string | Incoming HTTP content |

### Outputs
| Name | Type | Description |
|-- |-- |-- |
| headers | map | Map of HTTP headers for the request |
| http | string | Raw HTTP request |
| json | mixed | A JSON decoded value for the body if the content-type was JSON |
| method | string | HTTP method (e.g. GET, POST, PUT, DELETE) |
| params | map | A map of the query parameters (e.g. {these = \"params\"}) |
| path | string | HTTP path including query (e.g. /this-page?these=params) |
| query | string | Query string of request (e.g. \"?these=params\") |
| text | string | Raw body of the HTTP request |
| uri | string | Path without the query string (e.g. /this-page) |