locals {
  http = regex("^(?P<method>[A-Z]+) (?P<path>.*) HTTP/(?P<version>.*)$", var.http)
  _headers = [
    for x in split("\\n", trim(var.headers, "\\n")) :
    regex("^(?P<label>[^:]+):\\s?(?P<value>.*)", x)
  ]
  headers = {
    for x in local._headers :
    lower(x["label"]) => x["value"]
  }
  path    = regex("^(?P<uri>[^?]+)\\??(?P<query>.*)?$", local.http.path)
  _params = length(local.path.query) > 0 ? [for x in split("&", local.path.query) : regex("(?P<label>[^=]+)=(?P<value>[^&]+)", x)] : []
  params  = length(local._params) > 0 ? { for x in local._params : x["label"] => x["value"] } : {}
  json    = length(var.payload) > 0 && lookup(local.headers, "content-type", "") == "application/json" ? jsondecode(var.payload) : null
  text    = length(var.payload) > 0 ? var.payload : null
}

output "http" {
  value       = trimspace(var.http)
  description = "Raw HTTP request"
}

output "method" {
  value       = local.http.method
  description = "HTTP method (e.g. GET, POST, PUT, DELETE)"
}

output "path" {
  value       = local.http.path
  description = "HTTP path including query (e.g. /this-page?these=params)"
}

output "headers" {
  value       = local.headers
  description = "Map of HTTP headers for the request"
}

output "json" {
  value       = local.json
  description = "A JSON decoded value for the body if the content-type was JSON"
}

output "text" {
  value       = local.text
  description = "Raw body of the HTTP request"
}

output "uri" {
  value       = local.path.uri
  description = "Path without the query string (e.g. /this-page)"
}

output "query" {
  value       = local.path.query
  description = "Query string of request (e.g. \"?these=params\")"
}

output "params" {
  value       = local.params
  description = "A map of the query parameters (e.g. {these = \"params\"})"
}
