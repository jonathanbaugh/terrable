# Module: HTTP Response
This module will simplify creating the response string. It will automatically handle content-length
and HTTP status codes.

### Variables
| Name | Type | Description |
|-- |-- |-- |
| http | string | Incoming HTTP line (e.g. \"GET / HTTP/1.1\") |
| headers | string | Incoming HTTP headers |
| payload | string | Incoming HTTP content |

### Outputs
| Name | Type | Description |
|-- |-- |-- |
| headers | map | A map of HTTP headers. You should avoid passing content-length and content-type |
| content_type | string | (optional) The content-type of the HTTP response (default: application/json) |
| content | mixed | The content of a request. If the content_type is set to application/json it will jsonencode the content (default "") |
| status | integer | (optional) The status code for HTTP. Just the number is needed (default: 200) |
| allowed_methods | list | (optional) A list of allowed methods. Defaults to GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, and PATCH. (default: ["GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"]) |
| path | string | (optional) The required path for the endpoint. If this doesn't match, an empty string will be returned for the response. (default: "/") |
| uri | string | (optional) The request's URI (default: "/") |
| method | string | (optional) The method used in the HTTP request (default: "GET") |
| fallback | string | (optional) The fallback response if the paths do not match (default: "HTTP/1.1 404 Not Found\n\n") |

## Simple response
```
module "res" {
  source  = "path/to/http-response"
  content = { message = "Hello World!" }
}

output "response" {
  value = module.res.response
}
```

## Simple response for given path
```
# If the path doesn't match, it will return a 404 response
module "res" {
  source  = "path/to/http-response"
  path    = "/v1/hello"
  uri     = module.req.uri
  content = { message = "Hello World!" }
}

output "response" {
  value = module.res.response
}
```

## Simple response for method
```
# If the request isn't a GET request, it will return a 404 response
module "res" {
  source  = "path/to/http-response"
  allowed_methods = ["GET"]
  method = module.req.method
  content = { message = "Hello World!" }
}

output "response" {
  value = module.res.response
}
```

## Custom fallback response
```
module "res404" {
  source  = "path/to/http-response"
  status  = 404
  content = { error = "The page was not found" }
}

# If the request isn't a GET request, it will return a 404 response
module "res" {
  source   = "path/to/http-response"
  path     = "/v1/hello"
  uri      = module.req.uri
  content  = { message = "Hello World!" }
  fallback = module.res404.response
}

output "response" {
  value = module.res.response
}
```

## Separate responses for both GET and POST
```
module "res_get" {
  source   = "path/to/http-response"
  skip     = module.req.method != "GET" || module.req.uri != "/v1/hello"
  content  = { message = "Hello World! GET IT!" }
}

module "res_post" {
  source   = "path/to/http-response"
  skip     = module.req.method != "POST" || module.req.uri != "/v1/hello"
  content  = { message = "Hello World! Nice POST!" }
}

module "res_err" {
  source  = "path/to/http-response"
  skip    = "" != "${module.res_get.response}${module.res_post.response}"
  status  = 400
  content = { error = "You did something wrong" }
}

output "response" {
  value = "${module.res_get.response}${module.res_post.response}${module.res_err.response}"
}
```