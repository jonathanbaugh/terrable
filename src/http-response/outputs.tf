locals {
  # Make the header labels lower-case
  headers     = { for h in var.headers : lower(h.label) => h.value }
  headers_str = join("\n", [for h in var.headers : "${lower(h.label)}: ${h.value}"])

  # Default ot JSON but allow it to be overridden
  content_type = lookup(local.headers, "content-type", "content-type: ${var.content_type}")
  content      = local.content_type == "content-type: application/json" ? jsonencode(var.content) : tostring(var.content)
  status       = "${var.status} ${lookup(local.codes, var.status, "Status not found")}"
}

output "response" {
  description = "A full HTTP response based on the variables passed in"
  value = var.skip ? "" : (
    ! contains(var.allowed_methods, var.method) ? var.fallback_method : (
      length(var.uri) < length(var.path) || substr(var.uri, 0, length(var.path)) != var.path ? var.fallback_path :
  "HTTP/1.1 ${local.status}\n${local.content_type}\ncontent-length: ${length(local.content) + 1}\n${local.headers_str}\n\n${local.content}"))
}
