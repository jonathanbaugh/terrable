variable "headers" {
  description = "A map of HTTP headers. You should avoid passing content-length and content-type"
  default     = {}
}

variable "content_type" {
  description = "The content-type of the HTTP response"
  default     = "application/json"
}

variable "content" {
  description = "The content of a request. If the content_type is set to application/json it will jsonencode the content"
}

variable "status" {
  description = "The status code for HTTP. Just the number is needed."
  default     = 200
}

variable "allowed_methods" {
  description = "(optional) A list of allowed methods. Defaults to GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, and PATCH."
  default     = ["GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"]
}

variable "method" {
  description = "(optional) The method used in the HTTP request"
  default     = "GET"
}

variable "path" {
  description = "(optional) The required path for the endpoint. If this doesn't match, an empty string will be returned for the response."
  default     = "/"
}

variable "uri" {
  description = "(optional) The request's URI"
  default     = "/"
}

variable "fallback_path" {
  description = "(optional) The fallback response if the paths do not match"
  default     = "HTTP/1.1 404 Not Found\n\n"
}

variable "fallback_method" {
  description = "(optional) The fallback response if the method is not allowed"
  default     = "HTTP/1.1 405 Method Not Allowed\n\n"
}

variable "skip" {
  description = "(optional) If true, will return an empty response"
  default     = false
}
