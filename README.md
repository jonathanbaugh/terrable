# Terrable
This is a static site generator made with Terraform. It is an experiment to see what is possible
with Terraform. Use at your own risk.

## Rules
1. No programming languages. Only HCL, YAML, HTML, and CSS.
2. No SaaS solutions outside of vanilla providers.
3. No use of third party libraries or frameworks.

# Goals
- Have a static site deployed to https://terrable.work/
- Create an API with a Terraform backend
