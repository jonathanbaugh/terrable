FROM hashicorp/terraform:light
EXPOSE 1500
COPY . /code
WORKDIR /code
RUN apk update && apk add socat
ENTRYPOINT []
CMD ["/bin/sh", "./bin/server.sh", "src/api-debug"]
