#/bin/sh
cd $1

set -e
echo "Initializing module..."
terraform init > /dev/null
set +e

while true; do
  echo "Starting server on 0.0.0.0:1500..."
  socat TCP-LISTEN:1500,fork,reuseaddr SYSTEM:/code/bin/request.sh
  echo "Server stopped..."
done
