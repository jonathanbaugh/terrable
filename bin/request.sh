#/bin/sh

serve_500() {
  echo -ne "HTTP/1.1 500 Internal Server Error\n\nSomething went wrong."
  exit 1
}

read -r TF_VAR_http

CONTENT_LENGTH=0

while read -r line; do
  line=${line%%$'\r'}
  [ -z "$line" ] && break
  TF_VAR_headers="${TF_VAR_headers}\n${line}"
  if [ -z {$(echo "${line}" | grep -ei "^Content-Length:" 2>/dev/null)} ]; then
    CONTENT_LENGTH=$(echo $line | cut -d":" -f2 | tr -d '[:space:]')
  fi
done

if [[ "${CONTENT_LENGTH}" -gt 0 ]]; then
  read -n ${CONTENT_LENGTH} TF_VAR_payload
fi

export TF_VAR_http
export TF_VAR_headers
export TF_VAR_payload

TF_STATE="$RANDOM.tfstate"
export TF_CLI_ARGS_apply="-state=/tmp/${TF_STATE}"
export TF_CLI_ARGS_output="-state=/tmp/${TF_STATE}"

terraform apply -auto-approve -lock=false >> /tmp/log || serve_500
echo -ne "$(terraform output response)"
echo "[$(date)] $TF_VAR_http" >&2
rm -f $TF_STATE
